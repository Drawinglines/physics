# -*- coding: utf-8 -*-
"""
Created on Wed Aug 31 21:45:23 2016

@author: Administrator
"""



import terraai_preprocessing.preprocessing.pp_main as pp_main
import numpy as np
from datetime import datetime
import calendar
from collections import defaultdict
from time import strptime , strftime
import MySQLdb
import json


NowDatestr = str(strftime("%Y-%m-%d"))  #replaces line 19 & 20


def establish_DBconn(schema="public_data"): 
    
    with open("config.json", 'r') as f:
        config = json.load(f)
    
    cnx = { 'host': config["host"],'user':config["user"],'password': config["password"]}    
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'])
    
    #cnx = { 'host': config["host"],'user':config["user"],'password': config["password"],'db': schema}    
    #conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor() 
    return conn,cursor
    
    
def diff_month(d1str,d2str):
    if type(d1str)==str:    
        d1 = datetime.strptime(d1str , '%Y-%m-%d')
    else:
        d1 = d1str
    if type(d2str)==str:  
        d2 = datetime.strptime(d2str , '%Y-%m-%d')
    else:
        d2=d2str
        
    try:
        return (d2.year - d1.year)*12 + d2.month - d1.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d2)
        return np.NaN





def add_months(sourcedatestr,months):
    num = len(months)
    if type(sourcedatestr)==str: 
        sourcedate = datetime.strptime(sourcedatestr , '%Y-%m-%d').date() 
    else:
        sourcedate = sourcedatestr
    month = [ sourcedate.month - 1 + months[i] for i in range(num) ]
    year  = [int(sourcedate.year + x / 12 ) for x in month]
    month = [ month[i] % 12 + 1 for i in range(num) ]
    day   = [min(sourcedate.day , calendar.monthrange(year[i],month[i])[1]) for i in range(num)]
    day   = [calendar.monthrange(year[i],month[i])[1] for i in range(num)] # get the end of month day number date
    dates = [datetime(year[i],month[i],day[i]).date() for i in range(num) ]    
    #datestring = [(str(year[i])+"-"+str(month[i])+"-"+str(day[i])) for i in range(len(year))]   
    #dates      = [datetime.strptime(datestring[i] , '%Y-%m-%d').date() for i in range(len(datestring))]
    return dates


def impute_missing_data(data_in):
    ''' Accepts a list of dictionaries and analyzes them one by one '''

    #LD_dataOut = LD_dataIn
    #for lnum,data_in in enumerate(LD_dataIn):

    # determine the number of month between first and last dates given in data
    well_start_date = data_in['report_date_mo'][0]
    month_no_indx = np.array([diff_month(well_start_date, x) for x in data_in['report_date_mo']])

    # generate the continuous month dates strings for the new data
    allindx = range(max(month_no_indx)+1)
    dates_obj = add_months(well_start_date, allindx)

    # imput zeros for missing data
    data_out = data_in.copy()
    impute_keys = ['report_date_mo', "oil_mo", "gas_mo", "prod_days"]

    if "water_mo" in data_in.keys():
        impute_keys.append("water_mo")
    else:
        impute_keys.append("wtr_mo")

    for key in impute_keys:
        # Update the measured values series by imputing NaNs when data is missing
        data_out[key] = np.empty(shape=len(dates_obj), dtype=type(data_in[key]))
        #np.array(data_in[key],dtype=type(data_in[key][0]),ndmin=len(dates_obj ))
        data_out[key].fill(0.0) # can change into zero if want 0

        if np.size(data_in[key]) == 1:
            length = len(data_out[key])

            #to handle wells with only one entry
            if np.size(data_in[key]) == np.size(data_out[key]):
                data_out[key] = data_in[key]
            else:
                data_out[key] = np.nan_to_num([data_in[key][0] for i in xrange(length)])

        else:
            data_out[key][month_no_indx] = np.nan_to_num(data_in[key])

        #==== replace nans with zeros
        data_out[key] = np.nan_to_num(data_out[key])

    #get linear time index of months and the continuous dates strings
    data_out['MonthIndx'] = np.asarray(range(max(month_no_indx)+1))  #range(month_no_indx[-1])
    data_out['report_date_mo'] = np.asarray(dates_obj)

    # Check that every key has list of data with equal length
    for key in data_out.keys():
        temp_m, temp_n = len(data_out[key]), len(data_out['report_date_mo'])

        if temp_m != temp_n:
            data_out[key] = [data_out[key][-1]]*temp_n

    return data_out
    
    
    
    
    
def get_Data(cursor, schema, tablename, apis, 
             StartDate='1900-01-01', EndDate=NowDatestr):
    #==== Getting the production data from DB ====
    Structure = "API_prod, report_date_mo, oil_mo, water_mo, gas_mo, \
                prod_days, lat, longi, pool_name_master, TotalDepth, \
                operator_name_master, SpudDate, FieldName"
    
    query = "SELECT " + Structure + " FROM " + schema + "." + tablename + " WHERE API_prod in ( " + ",".join(map(str,apis)) +  " ) order by report_date_mo ASC;"
    cursor.execute(query)
    row = cursor.fetchall()
    
      
    
    
    data = defaultdict(list)
    for (i, item) in enumerate(row):
        
        item = np.array(map(lambda x: 0.0 if x==None else x, item))
        
        #print item
        data['API_prod'].append( int(float(item[0])) )
        data['report_date_mo'].append( datetime.strptime(item[1],"%Y-%m-%d").date())
        data['oil_mo'].append( int(float(item[2])))
        data['water_mo'].append( int(float(item[3] )))
        data['gas_mo'].append( int(float(item[4] )))
        data['prod_days'].append( int(float(item[5] )))
        data['lat'].append(float(item[6] ))
        data['longi'].append( float(item[7]) )
        data['pool_name_master'].append( item[8])
        data['TotalDepth'].append( float(item[9]) )
        data['operator_name_master'].append( item[10] )
        #data['SpudDate'].append( item[11].strftime("%Y-%m-%d") )
        data['FieldName'].append( item[12])
    
    if bool(data):  
        data = impute_missing_data(data) # impute new data if some months are missing and add the corresponding dates  
    return data
    
    
#-----------------------------------------
# Establish the connections
conn, cursor = establish_DBconn()
unique_pids = [402908773 ,   402904174  ,  402900070   , 403005775]


#====== Read data
for api in unique_pids:
    well_data = get_Data(cursor, 'public_data', 'calidata', [api], 
                         StartDate='1900-01-01', EndDate=NowDatestr)
    print "well data extracted"
    
    
