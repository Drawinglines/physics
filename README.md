# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### ToDO: ###

* Add the static phyisical parameters to DB output after fitting (Done)
* Export forecasts for baseline model to DB for ND dataset (Done)
* Simplify the integral for NH model for faster fit
* debug the check valid producer error (Done)
* Add outlier detection idea to the code
* Rethink the logic around major events
* rewrite the testing and training indexes based on dates (Done)
* export testing dates as well (Done)
* Read Static Attr from Master Table and put in dict. (Done)
* Save results in on file after analysis
* code to push all the results at once