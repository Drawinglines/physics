# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 11:20:26 2015

@author: Reza_3
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Nov 07 10:18:26 2015

@author: Reza_3
"""
import numpy as np 
import scipy as sp
# added for minimization
from scipy.integrate import quad
from collections import defaultdict

"=============================================================================="

# =============================================================================
# Miscellaneous functions internally used here
# =============================================================================
def heaviside (x , x0):
    " sign function "
    u = x - x0
    output = 0.5*(np.piecewise(u, [u < 0, u >= 0], [-1, 1]) + 1.0 )
    " Heaviside step function "
    #output = 0.5*(np.sign(x-x0) + 1)
    return output
    
    
    
# =============================================================================
# Physics based Semi-Analytical Models
# =============================================================================
def Arps_exp(t, params):
    """ Arps exponential flow equation.
    Parameters:
    t -- a list containing the times. 
    Each element is a dictionary with matrix keys.
    params -- t0 starting time of series, q0 largest oil production rate, D decline rate

    Returns:
    q(t) -- oil production rate simulated
    """
    q0   = params['q0'].value
    D    = params['D'].value
    u0   = params['u0'].value
    
    #q = q0 * np.exp(-D*np.array(t)) #* heaviside (t , u0)
    q = np.multiply( (q0 * np.exp(-D*np.array(t-u0))) , heaviside (t , u0) )
    
    # Get the parameters field
    outparams = defaultdict(list)
    outparams['q0'] = q0
    outparams['D']  = D
    outparams['u0'] = u0
    
    return np.array(q) , np.array(t) , dict(outparams)
  
  
  
def Arps_Harmonic(t, params):
    """ Arps Harmonic flow equation.
    Parameters:
    t -- a list containing the times. 
    Each element is a dictionary with matrix keys.
    params -- t0 starting time of series, q0 largest oil production rate, D decline rate

    Returns:
    q(t) -- oil production rate simulated
    """    
    q0   = params['q0'].value
    D    = params['D'].value
    
    q    = (q0 / np.power( (1+D*(t)) , 1))
    return np.array(q) , np.array(t)
  
  
def Arps_Hyperbolic(t, params):
    """ Arps Hyperbolic flow equation.
    Parameters:
    t -- a list containing the times. 
    Each element is a dictionary with matrix keys.
    params -- t0 starting time of series, q0 largest oil production rate, D decline rate

    Returns:
    q(t) -- oil production rate simulated
    """
    q0   = params['q0'].value
    D    = params['D'].value
    b    = params['b'].value
    u0   = params['u0'].value
    
    
    '''
    print "u0 "+str(u0)
    print "b "+str(b)
    print "D "+str(D)
    print "q0 "+str(q0)
    '''
    power_term = np.array(np.power( np.abs(1+D*b*(t-u0)) , 1/b),dtype=np.longfloat)
    q          = np.multiply( np.longfloat(np.divide(q0 , power_term)) , heaviside (t , u0) )
    
    '''
    with warnings.catch_warnings():
        warnings.filterwarnings('error')
        try:
            q    = np.multiply( (q0 / np.power( np.abs(1+D*b*(t-u0)) , 1/b)) , heaviside (t , u0) )
            #warnings.warn(Warning())
        except Warning: 
            print "u0 "+str(u0)
            print "b "+str(b)
            print "D "+str(D)
            print "q0 "+str(q0)
            print 'Raised!'
            q    = np.multiply( (q0 / np.power( np.abs(1+D*b*(t-u0)) , 1/b)) , heaviside (t , u0) )
    
   '''
        
            
    #q = np.log(q+1.0)
    #q    = np.divide(float(q0) , np.array((1+np.dot(D*(t-u0),b))**(1/b)) ) 
    # Get the parameters field
    outparams = defaultdict(list)
    outparams['q0']   = q0
    outparams['D']    = D
    outparams['b']    = b
    outparams['u0']   = u0

    
    return np.array(q) , np.array(t) , dict(outparams)


def Power_Law(t, params):
    """ PowerLaw flow equation.
    Parameters:
    t -- a list containing the times. 
    Each element is a dictionary with matrix keys.
    params -- t0 starting time of series, q0 largest oil production rate, D decline rate

    Returns:
    q(t) -- oil production rate simulated
    """
    q0    = params['q0'].value
    Dinf  = params['Dinf'].value
    Di    = params['Di'].value
    n     = params['n'].value
    u0    = params['u0'].value
    
    # q = q0 * np.exp(-Dinf*(t-u0)-Di*((t-u0)**n))  
    q = np.multiply( (q0 * np.exp(-Dinf*(t-u0)-Di*((t-u0)**n))) , heaviside (t , u0) )
    
    # Get the parameters field
    outparams = defaultdict(list)
    outparams['q0']   = q0
    outparams['Dinf'] = Dinf
    outparams['Di']   = Di
    outparams['n']    = n
    outparams['u0']   = u0
    
    # calculate the jacobi matrix as well

    
    
    
    return np.array(q) , np.array(t) , dict(outparams)
    

def Stretched_Exp(t, params):
    """ Stretched Exp flow equation.
    Parameters:
    t -- a list containing the times. 
    Each element is a dictionary with matrix keys.
    params -- t0 starting time of series, q0 largest oil production rate, D decline rate

    Returns:
    q(t) -- oil production rate simulated
    """
    q0    = params['q0'].value
    b     = params['b'].value
    n     = params['n'].value
    
    q     = q0*np.exp(-(b*(t**n)))
    return np.array(q) , np.array(t)
    
    

def Ei_integral(a, b): 
    return quad(lambda x: (np.exp(x))/x , a , b)[0]
def f(x,c,telf,d): 
    return np.exp(-c*(x-telf)+d)
def g(x,bmin,bmax,Di,telf,c,d): 
    return 1/(1/Di + bmax*x - (bmax-bmin)/c * (sp.special.expi(-f(x,c,telf,d))-sp.special.expi(-f(0,c,telf,d))))
    
def norm_oilRate( x, bmin, bmax, Di, telf, c, d): 
    return  quad(g, 0, x, args=(bmin,bmax,Di,telf,c,d))[0]
    
def TransientHyper(t, params):
    """ Stretched Exp flow equation.
    Parameters:
    t -- a list containing the times. 
    Each element is a dictionary with matrix keys.
    params -- t0 starting time of series, q0 largest oil production rate, D decline rate
    Returns:
    q(t) -- oil production rate simulated
    """
    
    q0   = params['q0'].value
    telf = params['telf'].value
    bmax = params['bmax'].value
    bmin = params['bmin'].value
    Di   = params['Di'].value
    #t0   = params['t0'].value
    
    gamma= 0.57721566 # Euler-Mascheroni constant
    d    = np.exp(gamma);
    c    = d / (1.5*telf)
    # const_param = special.expn(1, -np.exp(c*telf+d))
    # ... compute some result based on u ...           
    
    #for i, u in enumerate(t):
    q = q0 * np.exp([ -1*norm_oilRate(u,bmin,bmax,Di,telf,c,d) for u in t])
    
    # extract the parameters
    # outparams  
         
   # Get the parameters field
    outparams = defaultdict(list)
    outparams['q0']   = q0
    outparams['telf'] = telf
    outparams['bmax'] = bmax
    outparams['bmin'] = bmin
    outparams['Di']   = Di
    
    return np.array(q) , np.array(t) , dict(outparams)
    

def SimulationModel(t,params):
    t = np.array(t)
    
    """ Calls the below semi-analytical phyiscs based model  and output simulated 
     q oil and x as times """
    #q , t , UsedParams = TransientHyper(t,params)
    #q , t , UsedParams = Stretched_Exp(t,params)
    
    # q , t ,UsedParams = Power_Law(t,params)    
    #q , t , UsedParams = Arps_exp(t,params)
    q , t , UsedParams = Arps_Hyperbolic(t,params)
    
    return np.array(q) , np.array(t) , dict(UsedParams)
    

    
    
    
    