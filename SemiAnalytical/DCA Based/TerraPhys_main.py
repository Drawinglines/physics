# -*- coding: utf-8 -*-
"""
Created on Tue Jul 05 12:25:55 2016

@author: Administrator
"""

import terraai_preprocessing.preprocessing.pp_main as pp_main
import TerraPhys_helpers as hp
import TerraPhys_corecomp as corecomp
from multiprocessing import  freeze_support
import datetime as dt
import multiprocessing 

#-----------------------------------------
# Establish the connections
conn,cursor = pp_main.establish_db_conn(pp_main.SCHEMA)
#-----------------------------------------
unique_pids = pp_main.get_well_ids (cursor, state_api=33, schema="public_data", db_prodtable_name="bigprod_ts")
unique_pids = unique_pids[0:100]
#unique_pids = [33007000350000]
NumofWells = len(unique_pids)
#========================= Global Parameters 
Parallel_Proc  = False
start_testdate = dt.datetime.strptime('2009-1-31' , '%Y-%m-%d').date() 
end_testdate   = dt.datetime.strptime('2015-12-31' , '%Y-%m-%d').date()  

# write params
wrTable  =  'temps'
wrSchema =  'results'


'''
def getData(well_pids):
    #====== Read data and make it global
    global dataorig
    dataorig = pp_main.start_preprocess(StateAPI=33, return_format="D_of_L", MP=False, Time_Trsh=2, Diff_days=2, well_IDs=well_pids)
    #return dataorig
'''        


def core_wellanalyzer(args):
    
    wellID,inAPI,trainID,teststartID,testendID,lenTS = args
    print ("Analyzing Well Number: "+str(wellID)+" outof("+str(NumofWells-1)+")      With name: "+str(long(inAPI)))
    #========== Analyze the data ==============================================
    _,_,AnsDict = corecomp.SingleWell_PhysicsBased_Regression(trainID , teststartID , testendID , lenTS , wellID)
    #==========================================================================
    # push the forecasts to DB
    pp_main.write_DB(conn,cursor,AnsDict,False,wrSchema,wrTable)
    
    return AnsDict



def multiwellproc():
    #====== Read data
    global dataorig
    dataorig = corecomp.getData(unique_pids)
    #====== Separate Training and Testing
    trainEndIDs , testStartIDs , testEndIDs , wellforeIDs , lens = hp.getTrainTest(dataorig , start_testdate, end_testdate)
    #====== Forward to computational core machine for Learning
    ansDict_list=[]
    #args = ((i,API,trainIDs[i],testIDs[i]) for i,API in enumerate(dataorig['API']) if i in wellforeIDs)
    inargs = ((ID,dataorig['API_prod'][ID][0],trainEndIDs[ID],testStartIDs[ID],testEndIDs[ID],lens[ID]) for j,ID in enumerate(wellforeIDs))
    
    
    
    if Parallel_Proc is True:
        print "Parallel Processing"
        PROCESSES = 4
        #===== using parallel processing
        pool  = multiprocessing.Pool(PROCESSES)
        tasks = pool.map_async(func = core_wellanalyzer, iterable = inargs , chunksize = 100)
        tasks.wait()
        #while (True):
        #    if (tasks.ready()): break
        #    remaining = tasks._number_left
        #    print "Waiting for", remaining, "tasks to complete..."
            #time.sleep(0.5)
        ansDict_tuple = tasks.get()
        pool.close()
        pool.join()
        ansDict_list = list(ansDict_tuple)
        del ansDict_tuple
    
    else:
        print "Sequential Processing"
        #===== using sequential processing
        for arg in inargs:
            ansDict_list.append(core_wellanalyzer(arg))    
  
    
    #====== Push the results to DB
if __name__ == "__main__":
    #freeze_support()
    #getData(unique_pids)
    multiwellproc()
    
        
    
    