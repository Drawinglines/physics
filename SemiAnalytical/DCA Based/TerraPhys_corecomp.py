# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 10:27:51 2015

@author: Reza_3
"""

#import cvxpy as cvx
from collections import defaultdict
import TerraPhys_helpers as hp

import cPickle as pk
import numpy as np
import TerraPhys_helpers as hp
import scipy as sp
from scipy import stats
import copy
# added for minimization
import lmfit
import ForwardSimModels as FWS
# added for statistics analysis
from scipy.stats.distributions import  t
# for confidence intervals
import numdifftools as numdiff
from scipy.signal import argrelmax # get the local maxima in an array


import terraai_preprocessing.preprocessing.pp_main as pp_main
def getData(well_pids):
    #====== Read data and make it global
    global dataorig
    dataorig,worthy_apis , notworthy_apis = pp_main.start_preprocess(state_api=33, return_format="D_of_L", mp_flag=True,
                     time_trsh=2, diff_days=2, verbosity=True, well_ids=well_pids)
    
    return dataorig




"=============================================================================="
def SingleWell_PhysicsBased_Regression(trainEndID, testStartID , testEndID , lenTS , wellID):
    """ Train a regression model using physics semianalytical multiple datasets.
        Parameters:
        -----------
    """
    out_dict = defaultdict(list)
    def log_normalization(in_ts):
        return np.log(in_ts+1.0)

    oil_trn = np.asarray(dataorig['oil_pd'][wellID][:trainEndID],dtype=np.longfloat) # get the oil data from training set
    oil_tst = np.asarray(dataorig['oil_pd'][wellID][testStartID:],dtype=np.longfloat) # get the oil data from testing  set
    len_trn = trainEndID

    len_tst = testEndID-testStartID
    #-------------------------------------------------------------------------
    # Outlier Detection
    mn, std    = np.mean(log_normalization(oil_trn)), np.std(log_normalization(oil_trn)) # mean and std of the data just to have



    # get the initial oil rate estimated
    indx    = next((i for i, x in enumerate(log_normalization(oil_trn)) if x>=0.8*mn), None) # x!= 0 for strict match
    init_q0 = float(np.max(oil_trn[:-5])) # float(oil_trn[indx]) # float(np.max(oil_Trn))
    init_u0 = np.argmax(oil_trn[:-5])  # np.argmax(oil_trn)    #indx

    #=== last local maxima
    #init_q0 = oil_Trn[argrelmax(oil_Trn)[0]]

    fit_params = lmfit.Parameters()
    #EMC = 0.57721566 # Euler-Mascheroni Constant
    #            (Name,  Value,  Vary,   Min,  Max,  Expr)
    fit_params.add('u0', value=init_u0 , vary=True, min=-1 , max=len(oil_trn) , expr=None)
    fit_params.add('q0', value=init_q0 , vary=True, min=0.0 , max=max(oil_trn)*10.0 , expr=None)
    fit_params.add('telf', value=1.2 , vary=True, min=0.0 , max=len_trn , expr=None)
    fit_params.add('bmax', value=2 , vary=True, min=0.0 , max=100. , expr=None)
    fit_params.add('bmin', value=0.5 , vary=True, min=0.0 , max=100. , expr=None)
    fit_params.add('Di', value=2.0 , vary=True, min=0.0 , max=100. , expr=None)
    fit_params.add('Dinf', value=2.0 , vary=True, min=0.0 , max=100. , expr=None)
    fit_params.add('D', value=2.0 , vary=True, min=0.0001 , max=100. , expr=None)
    fit_params.add('b', value=2.0 , vary=True, min=0.001 , max=10. , expr=None)
    fit_params.add('n', value=1 , vary=True, min=0.0 , max=5. , expr=None)
    fit_params.add('c', value=2.5 , vary=True, min=0.0 , max=5. , expr='exp(0.57721566)/(1.5*telf)')


    #--------------------------------------------------------------------------------------------------
    # fit the model
    x_trn = range(len_trn)


    # Update the fitting parameters to be used for fitting
    _,RelevantParams = residual(fit_params, x_trn, data=None)
    CommonKeys       = (RelevantParams.keys() or fit_params.keys()) # common keywords between the two
    Used_Params      = lmfit.Parameters()
    for i,item in enumerate(CommonKeys):
        Used_Params.add(fit_params[item])


    WEIGHTS = np.abs(np.log(oil_trn.astype(float)+2.0))

    # create Minimizer
    mini = lmfit.Minimizer(residual, Used_Params, (x_trn , oil_trn , WEIGHTS),scale_covar =True)

    # first solve with Nelder-Mead
    out1 = mini.minimize(method='Nelder' , params=Used_Params )
    #out1 = mini.minimize(method='tnc')
    #out1 = mini.minimize(method='cg')
    out2 = mini.minimize(method='tnc' , params = out1.params)


    #------------------------------------------

    #------------------------------------------
    # Forecast on Testing Period
    num_tst    = len_tst+1
    x_tst      = range(num_tst)

    oil_hat,UsedParams = residual(out2.params, x_tst)
    #------------------------------------------
    # Confidence Intervals
    #------------------------------------------
    # get the initial values for parameter vector
    #popt = [out_lsq.params[item].value for i,item in enumerate(out_lsq.params)]
    #lower_params, upper_params, CI_Oil_lower, CI_Oil_upper =  Confidence_Interval_Calc(np.abs(out_lsq.covar) , oil_Trn , popt , out_lsq.params , x_test)
    #testPeriod_oil = np.concatenate((oil_Trn , oil_Tst),0)
    WEIGHTS_Tst = np.abs(np.log(oil_hat.astype(float)+2.0))

    Predband_plow , Predband_phigh , Predband_std = Confidence_numerical(residual , out2.params , x_trn \
    , x_tst , oil_trn , oil_tst , oil_hat , Weights_trn=WEIGHTS , Weights_tst=WEIGHTS_Tst)




    #======== impute testing period attributes
    API_list         = np.array([dataorig['API_prod'][wellID][0] for i in xrange(num_tst)])
    tst_dates_mo     = np.array(hp.add_months(dataorig['report_date_mo'][wellID][testStartID],months=x_tst))
    tst_proddays     = np.array([dataorig['proddays'][wellID][i] for i in x_trn]+[float(tst_dates_mo[j].day) for j in x_tst if j >= len(x_trn)])
    oil_data         = dataorig['oil_pd'][wellID] + [0.0 for i in x_tst if i>len(x_trn)]
    wtr_data         = dataorig['wtr_pd'][wellID] + [0.0 for i in x_tst if i>len(x_trn)] 
    len_new = len(oil_data)
    #======== oil predictions
    out_dict['API_prod']        = [API_list]
    out_dict['report_date_mo']  = [tst_dates_mo]
    #out_dict['UsedParams']        = UsedParams
    out_dict['proddays']        = [tst_proddays]

    out_dict['oil_pd']= [oil_data]
    out_dict['oil_pd_fore_mean']= [oil_hat]
    out_dict['oil_pd_fore_p10'] = [Predband_plow]
    out_dict['oil_pd_fore_p90'] = [Predband_phigh]
    out_dict['oil_pd_fore_std'] = [Predband_std]


    #======== water predictions
    out_dict['wtr_pd'] = [wtr_data]
    out_dict['wtr_pd_fore_mean'] = [np.zeros(len_new)]
    out_dict['wtr_pd_fore_p10']  = [np.zeros(len_new)]
    out_dict['wtr_pd_fore_p90']  = [np.zeros(len_new)]
    out_dict['wtr_pd_fore_std']  = [np.zeros(len_new)]


    #------------------------------------------
    return np.array(oil_hat),np.array(oil_tst), out_dict






def Zmat_calculate(ydata):
    Z = np.diag(1.0*(ydata>0))
    return Z





def residual(pars, x, data=None, eps=None):
    """ Calculates the l2 norm of mismatch trying to minimize """
    model,monthNum,UsedParams = FWS.SimulationModel(x,pars)
    if data is None:
        return np.ndarray.astype(model,float) , UsedParams

    # build the Z matrix just like in trend filter for analysis of non zeros
    yobs = np.ndarray.astype(data,float)

    Z    = Zmat_calculate(yobs)
    # For robust optimization w.r.t outliers
    #objvals = np.sum(np.abs(objvals))
    objvals = np.dot(Z,(model - data))

    if eps is None: # if normalization vector is not provided
        return np.ndarray.astype(objvals,float)

    # provided normalization vector then normalize
    return np.ndarray.astype(np.divide(objvals,eps),float)



def Confidence_Interval_Calc(pcov , Ydat , popt , params , x_test):
     # confidence intervals
    n = len(Ydat)    # number of data points
    p = len(popt)    # number of parameters
    dof   = max(0, n - p) # number of degrees of freedom
    alpha = 0.1 # 90% confidence interval = 100*(1-alpha)
    # student-t value for the dof and confidence level
    tval  = t.ppf(1.0-alpha/2., dof)
    lower = []
    upper = []
    for p,var in zip(popt, np.diag(pcov)):
        sigma = var**0.5
        lower.append(p - sigma*tval)
        upper.append(p + sigma*tval)

    # save in parameters class of lmfit for consistency with the rest of the code
    lower_params  = copy.deepcopy(params)
    upper_params = copy.deepcopy(params)
    # insert parameters updated values to params
    for i,item in enumerate(lower_params):
        lower_params[item].value = lower[i]
        upper_params[item].value = upper[i]

    # ----------------------------------------------
    # get the prediction bands (confidence bands)

    # dont let u0 to deviate
    lower_params['u0'].value = copy.copy(params['u0'].value)
    upper_params['u0'].value = copy.copy(params['u0'].value)

    CI_Oil_lower,_ = residual(lower_params, x_test)
    CI_Oil_upper,_ = residual(upper_params, x_test)
    # ----------------------------------------------

    return lower_params , upper_params , CI_Oil_lower , CI_Oil_upper





def Confidence_numerical(func , opt_params , x_trn , x_tst , Y_trn , Y_tst , Y_est, Weights_trn=None , Weights_tst=None):
    # this function calculates the confidence intervals with the correct mathematics

    # get the initial values for parameter vector
    temp_params = copy.deepcopy(opt_params)
    p0          = [opt_params[item].value for i,item in enumerate(opt_params)]


    n = len(x_trn)    # number of data points
    p = len(p0)    # number of parameters
    dof   = max(0, n - p) # number of degrees of freedom
    confidence_level = 90 # in percent
    alpha = 1.-(confidence_level/100.) # 90% confidence interval = 100*(1-alpha)
    critical_probability = 1.0-alpha/2.0#P*

    # student-t value for the dof and confidence level
    tz_score  = t.ppf(critical_probability, dof)  # for t distribution and more accurate
    #tz_score  = sp.stats.norm.ppf(critical_probability)




    #---------------------------------------------
    # get the prediction bands
    # write a function with input arrays instead of params class in lmfit
    def residual_scipy(x_params , *argins):
        indep_x = argins[0][0]
        data    = argins[0][1]
        W       = argins[0][2]
        # insert x_array values to params
        for i,item in enumerate(temp_params):
            temp_params[item].value = x_params[i]
        # calculate the objective func value
        res = func(temp_params, indep_x, data=data, eps=W)
        return np.array(res.astype(float))

    def objective_fun(x_params , *argins):
        data    = argins[0][1]
        indep_x = argins[0][0]
        W       = argins[0][2]
        # insert x_array values to params
        for i,item in enumerate(temp_params):
            temp_params[item].value = x_params[i]
        # calculate the objective func value
        res = func(temp_params, indep_x, data=data, eps=W)
        obj_val = np.dot(0.2,np.linalg.norm(res,2)**2)
        return np.array(obj_val.astype(float))


    # calcuulate JACOBI matrix using the numdifftools package
    J_fun    = numdiff.Jacobian(residual_scipy)
    JACOBI   = J_fun(p0 , (x_tst,Y_est,Weights_tst)) # jacobi matrix
    JACOBI_T = np.transpose(JACOBI)

    #if len(Y_trn)!=len(Y_tst):
    #    print "hello"

    F   = J_fun(p0 , (x_trn,Y_trn,Weights_trn))
    F_T = np.transpose(F)


    # calculate hessian matrix
    #Hess_fun = numdiff.Hessian(objective_fun)
    #Hessian  = Hess_fun(p0 , (x_tst,Y_tst)) # Hessian matrix
    #Hessian  = np.dot(np.transpose(JACOBI),JACOBI)



    #obj_val    = objective_fun(p0 , (x_tst,Y_tst)) # residual vector
    res        = func(temp_params, x_trn, Y_trn, eps=Weights_trn)

    tempMat = np.linalg.pinv(np.dot(F_T,F))
    '''
    try:
        tempMat = np.linalg.inv(np.dot(F_T,F))
    except:
        tempMat = np.linalg.pinv(np.dot(F_T,F))
    '''

    # calculate confidence intervals
    s       = np.sqrt(1.0/(n-p)*(np.linalg.norm(res,2)**2))
    #print s
    tempVec = np.diagonal(np.eye(len(x_tst),len(x_tst),0) + np.dot(np.dot(JACOBI,tempMat),JACOBI_T) , 0)
    Predband_std   = np.dot(s , tempVec)
    Predband_lower = np.maximum(Y_est - tz_score*Predband_std , 0.0) # production rate cannot be below zero
    Predband_upper = Y_est + tz_score*Predband_std
    #-------------------------------------------------


    '''
    #-------------------------------------------------
    # get the confidence intervals on the parameters
    diag_vec = np.abs(np.diagonal(normCov_params,0)) # diag(A)
    CI_lower = p0 - tval*s*np.power(diag_vec,0.5)
    CI_upper = p0 + tval*s*np.power(diag_vec,0.5)
    # save in parameters class of lmfit for consistency with the rest of the code
    lower_params  = copy.deepcopy(opt_params)
    upper_params = copy.deepcopy(opt_params)
    # insert parameters updated values to params
    for i,item in enumerate(lower_params):
        lower_params[item].value = CI_lower[i]
        upper_params[item].value = CI_upper[i]
        # dont let u0 to deviate
    lower_params['u0'].value = copy.copy(opt_params['u0'].value)
    upper_params['u0'].value = copy.copy(opt_params['u0'].value)
    #-------------------------------------------------
    '''
    return Predband_lower , Predband_upper , Predband_std #, lower_params , upper_params



def wrapper_scipyOptim(res_fun, indep_vars, Ydat , params, method='lgmres'):

    temp_params = copy.deepcopy(params)
    Z    = Zmat_calculate(Ydat) # matrix for excluding outlier points to be optimized
    Ydat = np.dot(Z,Ydat)

    # Define internal objective function
    def ObjectiveFun(x_indep , *x_params):
        # insert x_array values to params
        for i,item in enumerate(temp_params):
            temp_params[item].value = x_params[i]

        # calculate the objective func value
        y,_,_ = FWS.SimulationModel(indep_vars,temp_params)
        #resdual_vec = res_fun(params, x_indep, Ydat, eps=None)
        #objFunval   = float(0.5*np.dot(resdual_vec,resdual_vec))
        y = np.dot(Z,y)
        return np.array(y.astype(float))


    def residual_scipy(x_params , *argins):

        data    = argins[1]
        indep_x = argins[0]
        # insert x_array values to params
        for i,item in enumerate(temp_params):
            temp_params[item].value = x_params[i]

        # calculate the objective func value
        res = residual(temp_params, indep_x, data=data, eps=None)

        return np.array(res.astype(float))


    # get the initial values for parameter vector
    p0 = [params[item].value for i,item in enumerate(params)]

    WEIGHTS = np.ndarray.astype((Ydat+1.0)**0.5,float)

    # solve the optimization problem
    #results = sp.optimize.newton_krylov(F=ObjectiveFun, xin=x0)
    popt , pcov = sp.optimize.curve_fit(f=ObjectiveFun, xdata=indep_vars.astype(float), ydata=Ydat.astype(float), p0=p0)
    #popt , pcov = sp.optimize.leastsq(func=residual_scipy, args=(indep_vars.astype(float),Ydat.astype(float) ), x0=p0 , diag=WEIGHTS )

    #------------------------------
    # prepare the outputs and CI
    #------------------------------
    # insert parameters updated values to params
    opt_params = copy.deepcopy(params)
    for i,item in enumerate(opt_params):
            opt_params[item].value = popt[i]


    lower_params , upper_params , lower , upper = Confidence_Interval_Calc(pcov , Ydat , popt , params)

    # confidence time series (curves)
    CI_lower     = ObjectiveFun(indep_vars , *lower)
    CI_upper     = ObjectiveFun(indep_vars , *upper)

    # output structure
    results = lmfit.Minimizer(residual, [], ([] , []))
    results.params = opt_params
    results.cov    = pcov;

    return results , lower_params , upper_params



###############################################################################

if __name__ == '__main__':
    #getData([])
    print "Core Computational Unit Started" 




