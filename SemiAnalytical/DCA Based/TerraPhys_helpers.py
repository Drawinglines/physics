# -*- coding: utf-8 -*-
"""
Created on Thu Jul 07 11:32:33 2016

@author: Administrator
"""

from datetime import datetime
import numpy as np
import calendar





def diff_month(d1str,d2str):
    if type(d1str)==str:    
        d1 = datetime.strptime(d1str , '%Y-%m-%d')
    else:
        d1 = d1str
    if type(d2str)==str:  
        d2 = datetime.strptime(d2str , '%Y-%m-%d')
    else:
        d2=d2str
        
    try:
        return (d2.year - d1.year)*12 + d2.month - d1.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d2)
        return np.NaN





def add_months(sourcedatestr,months):
    num = len(months)
    if type(sourcedatestr)==str: 
        sourcedate = datetime.strptime(sourcedatestr , '%Y-%m-%d').date() 
    else:
        sourcedate = sourcedatestr
    month = [ sourcedate.month - 1 + months[i] for i in range(num) ]
    year  = [int(sourcedate.year + x / 12 ) for x in month]
    month = [ month[i] % 12 + 1 for i in range(num) ]
    day   = [min(sourcedate.day , calendar.monthrange(year[i],month[i])[1]) for i in range(num)]
    day   = [calendar.monthrange(year[i],month[i])[1] for i in range(num)] # get the end of month day number date
    dates = [datetime(year[i],month[i],day[i]).date() for i in range(num) ]    
    #datestring = [(str(year[i])+"-"+str(month[i])+"-"+str(day[i])) for i in range(len(year))]   
    #dates      = [datetime.strptime(datestring[i] , '%Y-%m-%d').date() for i in range(len(datestring))]
    return dates

       

def getTrainTest(data_DofL , start_testdate='2009-1-31' , end_testdate='2015-12-31'):
    numwells = len(data_DofL['report_date_mo']) # length of the well time series data 
    
    
    
    
    testStartIDs, trainEndIDs , lens , wellforeIDs , testEndIDs = list(), list(), list(), list(), list()
    for i in range(numwells):
        lens.append(len(data_DofL['report_date_mo'][i]))
        #matchings = [indx for indx,s in enumerate(data_DofL['dates'][i].tolist()) if start_testdate==s]
       
        # calculate number of month between start and end date of testing
        indx_tst_end = diff_month(data_DofL['report_date_mo'][i][0],end_testdate)
    
        testSet   = data_DofL['report_date_mo'][i]>=start_testdate
        matchings = [j for j, x in enumerate(testSet) if x]
        
        if sum(testSet)!=0:
            testStartIDs.append(0) # matchings[0]: for strict testing period
            testEndIDs.append(indx_tst_end) # relative index of end of testing period (could be larger than time series length)
            trainEndIDs.append(matchings[0])
            wellforeIDs.append(i)
        else:
            trainEndIDs.append(lens[-1])
            testStartIDs.append(0)
            testEndIDs.append(indx_tst_end)
            
    return trainEndIDs , testStartIDs , testEndIDs , wellforeIDs , lens
    
    
#if __name__ == "__main__":
    #dat = datetime(2009,1,31).date()
    #dat2=add_months(dat , [1])
    #print dat2
    