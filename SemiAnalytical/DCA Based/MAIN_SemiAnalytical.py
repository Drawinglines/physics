'''
###############################################################################
#################### Written By @ Reza Khaninezhad March 2016 #################
###############################################################################
'''

# my modules
import terraai_preprocessing.preprocessing.pp_main as pp_main

# python modules visualization
from collections import defaultdict
from bprofile import BProfile
from datetime import datetime
import time


#from multiprocessing import Pool




#------------------------------
# parameterz and initialization
NowDate = datetime.strftime(datetime.now(),'%Y-%m-%d')
testDate = pp_main.diff_month(datetime.strptime('2009-01-01' , '%Y-%m-%d'))


#=== testing period
TestEndDate     = "2009-01-01"
StateAPI        = 33   # for NorthDakuta = 33


#-----------------------------------------
#-----------------------------------------
# Establish the connections
conn,cursor  = pp_main.establish_DBconn()
#----------------------------------------- 
# Drop table if exists
Wrtable_name = "Phys_v3_Semi_Hyper_Prod_NDK" 
Wrschema     = "results"
#-----------------------------------------



#------------------------------ Get unique APIs -------------------------------
# from tahas tabels
#unique_pids = reza_hp.get_WellPids_fromResultsTables(cursor, 'FFR_NDK_2000_36', 'results')

unique_pids = pp_main.get_WellPids(cursor,StateAPI=33,schema="public_data",DB_ProdTable_name="big_prod_sum2",DB_MasterTable_name="big_master")
#unique_pids = pp_main.get_TestingWellPids('data\\test_ids_33')
#unique_pids  = [4301330025]
#unique_pids = pp_main.get_TestingWellPids('data\\WellsWithErrorForReza.txt')
unique_pids = unique_pids[0:10]
NumofWells  = len(unique_pids) #max number of wells to be analysed





def Well_Analyzer(args):
    #i,inAPI,oilPrice_dict = args
    
    
    #=========== Wells to be analyzed
    print ("Analyzing Well Number: "+str(i)+" outof("+str(NumofWells-1)+")      With name: "+str(long(inAPI)))
    #========== Read Data From DB and preprocess them =========================
   
    #======= Get the whole data for this well
    dataorig = pp_main.start_preprocess(StateAPI, return_format="D_of_L", MP=True, Time_Trsh=2, Diff_days=2, wellIDs=unique_pids) 

    # ===== update the training and testing start dates
    TrainStartDate = dataorig['dates'][0]
    TestStartDate  = TrainStartDate  
    
    #======= Get the training data
    # get the start and end indeces of training set 
    Trn_startIndx = 0
    Trn_endIndx   = pp_main.diff_month(TrainStartDate,TrainEndDate)
    # get the training data
    dataTrn,_,_   = pp_main.Trim_Dictionary(dataorig,Trn_endIndx,Trn_startIndx)
    
    if len(dataTrn['oil'])==0:
        print 'Well Was not actived during training time'
        return
        
    #======= Get the Testing data
    # get the start and end indeces of testing set
    Tst_startIndx = Trn_startIndx
    Tst_endIndx   = reza_hp.diff_month(TestStartDate,TestEndDate)
    # get the testing data
    dataTst,_,_ = reza_hp.Trim_Dictionary(dataorig,Tst_endIndx,Tst_startIndx)
    
    
    #========== Analyze the data ==============================================
    #Physics Based Fitting
    _,_,AnsDict = core_physics.SingleWell_PhysicsBased_Regression(dataTrn , dataTst)
    #==========================================================================
    
    
    #========== Write Well By Well ============================================

    WrDict = defaultdict(list)
    
    WrDict['oil_pd']           = AnsDict['oil'] 
    WrDict['oil_pd_fore_mean'] = AnsDict['oil_pd_fore_mean'] 
    WrDict['oil_pd_fore_std']  = AnsDict['oil_pd_fore_std'] 
    WrDict['oil_pd_fore_p10']  = AnsDict['oil_pd_fore_p10']
    WrDict['oil_pd_fore_p90']  = AnsDict['oil_pd_fore_p90']
    
    WrDict['wtr_pd']           = AnsDict['water'] 
    WrDict['wtr_pd_fore_mean'] = AnsDict['water_pd_fore_mean']
    WrDict['wtr_pd_fore_std']  = AnsDict['water_pd_fore_std']
    WrDict['wtr_pd_fore_p10']  = AnsDict['water_pd_fore_p10']
    WrDict['wtr_pd_fore_p90']  = AnsDict['water_pd_fore_p90']
    
    WrDict['report_date_mo']  = AnsDict['Dates_Fore']
    WrDict['API_prod']        = AnsDict['API_prod']
    WrDict['proddays']        = AnsDict['proddays']

    #WrDict         = {key:AnsDict[key] for key in ['a', 'b']} 
    _,_ = DB_hp.Write_Dict_ToDB(WrDict, Wrschema, Wrtable_name, conn, cursor, Format="SQL")
    
    
    #==========================================================================   
    return AnsDict
        
'''        


'''
def phys_main(): 
    print "Starting Analysis for " + str(len(unique_pids))+" Wells"
    
    #========== Get the production data and pre-process them ==================
    dataorig = pp_main.start_preprocess(StateAPI,'L_of_D',unique_pids) 

    #========== Read Data From DB =============================================
    ansDict_list  = list()
    args = ((i,API) for i,API in enumerate(unique_pids))
         
       
    #===== using parallelprocessing
    
    pool  = Pool()
    tasks = pool.map_async(Well_Analyzer, args)
    tasks.wait()
    while (True):
        if (tasks.ready()): break
        remaining = tasks._number_left
        print "Waiting for", remaining, "tasks to complete..."
        #time.sleep(0.5)
    ansDict_tuple = tasks.get()
    pool.close()
    pool.join()
    ansDict_list = list(ansDict_tuple)
    del ansDict_tuple
    
 
    #===== using sequential processing
    
    for i,arg in enumerate(args): #NumofWells
            ansDict_list.append(Well_Analyzer(arg)) 
      
          
    #===== remvoe none elements and change to tuple   
    ansDict_list  = [x for x in ansDict_list if x is not None]
    ansDict_tuple = tuple(ansDict_list)   
    del ansDict_list        
    
    #======= save results into cpickle or jason file ======================
    #with open('results/NorthDakuta_L1DeTrend_tuple.p', 'wb') as fp:
    #    pk.dump(AnsDict_tuple, fp)
    print "Analyzed and Saved Data for " + str(len(ansDict_tuple))+" Wells"
    #====================================================================== 
    
    #======= Close the DB connection
    cursor.close()
    conn.close()
    # time took the code
    EndTime = time.time()
    delt_t  = EndTime-start_time
    print ("Total Execution Time: ("+str(delt_t/60.0) + ") Minutes")

    
    #return WrDict_tuple
    return


'''       
        
if __name__ == "__main__":
#    profiler = BProfile('L1DeTrending.png')
#with profiler:
#         Phys_main()       
    #phys_main() 
    dataorig = pp_main.start_preprocess(StateAPI,'L_of_D',unique_pids) 
    
    
        

        



